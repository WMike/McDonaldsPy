import getpass
import mcdonalds

while True:
	try:
		file = open("email.txt", "r") 
		username = file.read()
	except:
		username = input("Your email: ")
	password = getpass.getpass("Your password: ")

	mcd = mcdonalds.McDonalds("at")
	if mcd.login(username, password)["status"] == "success":
		print("Logged in successfully!")
		break
	else:
		print("Error occured, please try again ("+mcd.login(username, password)["reason"]+")")
		continue

while True:
	print("What do you want to do?")
	print("1. Get Cards")
	print("2. Get Ms")
	print("3. Get personal info")
	print("4. Get rewards")
	print("5. Get QR-Code")
	print("6. Save email")
	option = input("?: ")
	if option == "debug":
		print(mcd.get_info())
	elif option == "1":
		mcd.get_cards(option="print")
	elif option == "2":
		print("Your Ms: " + str(mcd.get_ms()))
	elif option == "3":
		personal = mcd.get_personal()
		print("Your Info:")
		print("  Name: "+personal["first"] + " " + personal["last"])
		print("  Email: "+personal["email"])
	elif option == "4":
		mcd.get_rewards(option="print")
	elif option == "5":
		print(mcd.get_qr())
	elif option == "6":
		print(mcd.save_email())