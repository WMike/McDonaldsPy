import requests as r
from json import loads
from urllib.parse import urlencode
from pyqrcode import create as qrcreate

class McDonalds:
	def __init__(self, countrycode):
		self.countrycode = countrycode
	def login(self,username, password):
		#LOGIN
		headers = {
			"Authorization": "Basic YXBwOmluY2FyY2VybyUyMw==",
			"Content-Type": "application/json; charset=utf-8",
			"Accept-Encoding": "gzip",
			"User-Agent": "okhttp/2.3.0"
		}

		info = {
			"username": username,
			"password": password
		}

		try:
			req = r.post("https://app.my.mcdonalds."+self.countrycode+"/iqoneapi/oauth/token?"+urlencode(info)+"&scope=read,write,trust&grant_type=password", headers=headers)
			if req.status_code == 500:
					return {"status": "error", "reason": "server_error", "long": "The McDonalds server encountered an error while processing your request"}
		except Exception as e:
			return {"status": "error", "reason": "network_error", "long": "An error with the network has occured.","error": e}
		jsn = loads(req.text)
		try:
			self.username = username
			self.token = jsn["access_token"]
			return {"status": "success", "token": jsn["access_token"]}
		except:
			if req.status_code == 400 and jsn["error_description"] == "Bad credentials":
				return {"status":"error", "reason": "invalid_credentials", "long": "Email or password invalid!"}
			else:
				return {"status":"error", "reason": "unknown_error", "long": "An unknown error occured!"}

	def get_info(self):
		url = "https://app.my.mcdonalds."+self.countrycode+"/iqoneapi/app/customer"
		headers = {
			"Authorization": "Bearer "+self.token,
			"Host": "app.my.mcdonalds."+self.countrycode,
			"Connection": "Keep-Alive",
			"Accept-Encoding": "gzip",
			"User-Agent": "okhttp/2.3.0"
		}

		req = r.get(url, headers=headers)
		jsn = loads(req.text)
		return jsn

	def get_cards(self, option="return"):
		jsn = self.get_info()
		try:
			cards = jsn["loyaltyCards"]
			if cards == []:
				return "error"
			if option == "return":
				return cards
			elif option == "print":
				for count,i in enumerate(cards):
					print("Card #"+str(count+1))
					print("  Name:         "+i["uniqueName"])
					print("  ID:           " + str(i["id"]))
					print("  Online-Coins: " + str(i["pointCountOnline"]))
					print("  Virtual:      "+str(i["virtual"]))
		except:
			return "error"

	def get_ms(self):
		jsn = self.get_info()
		points = jsn["customerStatus"]["loyaltyPoints"]
		return points

	def get_personal(self):
		jsn = self.get_info()
		info = {
			"first": str(jsn["firstName"]),
			"last": str(jsn["lastName"]),
			"email": str(jsn["email"]),
			"birthdate": str(jsn["birthDate"]),
			"gender": str(jsn["gender"])
		}
		return info

	def get_rewards(self, option="return"):
		jsn = self.get_info()
		rewards = jsn["customerStatus"]["rewards"]
		if option == "return":
			return rewards
		elif option == "print":
			for i in rewards:
				print("Reward #"+str(i["id"]))
				print("  Title: "+str(i["name"]))
				print("  Picture: "+str(i["url"]))
				print("  Cost: "+str(i["loyaltyPoints"]))
			return
	def get_qr(self):
		jsn = self.get_info()
		card = jsn["loyaltyCards"][0]["uniqueName"]
		mcd_qr = qrcreate(card)
		mcd_qr = mcd_qr.terminal(quiet_zone=1)
		mcd_qr = mcd_qr.replace("[7m", "▀")
		mcd_qr = mcd_qr.replace("[0m", " ")
		mcd_qr = mcd_qr.replace("[49m", " ")
		mcd_qr = mcd_qr.replace(" ", "")
		return mcd_qr

	def save_email(self, file="email.txt"):
		try:
			with open(file, 'w') as file:
				try:
					file.write(self.username)
					return "Done"
				except:
					raise
		except:
			raise